import React, { Component } from 'react';
import { Text, StyleSheet, Dimensions, Button, View, AsyncStorage } from 'react-native';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  text: {
    fontSize: 17,
    marginTop: 10,
  },
  title: {
    fontSize: 27,
    textAlign: 'center',
  },
});

class Room extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      temp: '',
      light: false,
      presence: false,
    }
  }

  async componentDidMount() {
    try {
      let value = await AsyncStorage.getItem('@ApiToken:key');
      if (value !== null){
        this.setState({ validUser: value });

        fetch('https://homenet.erik.cat/api/app/room', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            api_token: value,
            id: this.props.navigation.state.params.id,
          })
        })
         .then((response) => response.json())
         .then((responseJson) => {
           if (responseJson.message) console.warn(responseJson.message);
           else if (responseJson.error) console.warn(responseJson.error);
           else {
             this.setState({ name: responseJson.name, temp: responseJson.temp, light: responseJson.light, presence: responseJson.presence });
           }
         })
         .catch((error) => { console.error(error); });
      }
    } catch (error) {
      console.error(error.message);
    }
  }

  handleToggleLights() {
    this.setState({ light: !this.state.light });

    fetch('https://homenet.erik.cat/api/app/set_room_light', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        api_token: this.state.validUser,
        id: this.props.navigation.state.params.id,
        light: this.state.light ? 0 : 1,
      })
    })
     .then((response) => response.json())
     .then((responseJson) => {
       if (responseJson.meemailssage) this.setState({ error: responseJson.message });
       else if (responseJson.error) this.setState({ error: responseJson.error });
     })
     .catch((error) => { console.error(error); });
  }

  render() {
    return (
      <View style={{ width: width * 0.8, alignSelf: 'center', marginTop: 20}}>
        <Text style={styles.title}>{this.state.name}</Text>
        <View  style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <View style={{ width: '40%', marginVertical: 20 }}>
            <Text style={styles.text}>Lights are {this.state.light ? "on" : "off"}</Text>
          </View>
          <View style={{ width: '50%', marginVertical: 20 }}>
            <Button onPress={() => this.handleToggleLights()} title={this.state.light ? "Turn them off" : "Turn them on"} />
          </View>
        </View>
        <Text style={styles.text}>Current temperature of {this.state.temp}ºC</Text>
        <Text style={styles.text}>There is {this.state.presence ? "someone" : "nobody"} in the room</Text>
      </View>
    );
  }
};

export default Room;
