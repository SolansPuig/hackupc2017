import React, { Component } from 'react';
import { Text, StyleSheet, Dimensions, Button, View, AsyncStorage, Switch, TextInput, Picker, Slider } from 'react-native';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  text: {
    fontSize: 17,
    marginTop: 10,
  },
  title: {
    fontSize: 27,
    textAlign: 'center',
  },
});

class HouseSettings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      api_token: '',
      id: '',
      set_temp: 25,
      room_id: '',
      room_id_bool: false,
      rooms: [],
      advanced_settings: false,
      histeresys: 5,
    }
  }

  async componentDidMount() {
    try {
      let value = await AsyncStorage.getItem('@ApiToken:key');
      if (value !== null){
        this.setState({ api_token: value });

        fetch('https://homenet.erik.cat/api/app/info', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            api_token: value,
          })
        })
         .then((response) => response.json())
         .then((responseJson) => {
           if (responseJson.message) console.warn(responseJson.message);
           else if (responseJson.error) console.warn(responseJson.error);
           else {
             this.setState({ id: responseJson.id, set_temp: responseJson.house.set_temp, room_id: responseJson.house.room_id, hysteresis: responseJson.house.hysteresis_threshold });
             if (this.state.room_id) this.setState({ room_id_bool: true });
             else this.setState({ room_id_bool: false });
           }
         })
         .catch((error) => { console.error(error); });

         fetch('https://homenet.erik.cat/api/app/rooms', {
           method: 'POST',
           headers: {
             'Accept': 'application/json',
             'Content-Type': 'application/json',
           },
           body: JSON.stringify({
             api_token: value,
           })
         })
          .then((response) => response.json())
          .then((responseJson) => {
            if (responseJson.message) console.warn(responseJson.message);
            else if (responseJson.error) console.warn(responseJson.error);
            else {
              this.setState({ rooms: responseJson });
            }
          })
          .catch((error) => { console.error(error); });
      }
    } catch (error) {
      console.error(error.message);
    }
  }

  getRooms() {
    return this.state.rooms.map((room) => {
      let id = room.id;
      return (
        <Picker.Item key={id} label={room.name} value={id} />
      );
    })
  }

  handleChangeTemp(text) {
    this.setState({ set_temp: parseInt(text) });
  }

  handleChangeRoomIdBool(value) {
    this.setState({ room_id_bool: value });

    if (!value) {
      this.setState({ room_id: null });
    }
  }

  handleChangeRoomId(value) {
    if (value != 0) this.setState({ room_id: parseInt(value) });
    else this.handleChangeRoomIdBool(false);
  }

  handleToggleAdvancedSettings(value) {
    this.setState({ advanced_settings: value });
  }

  handleChangeHysteresis(value) {
    this.setState({ hysteresis: value });
  }

  handleSend() {
    fetch('https://homenet.erik.cat/api/app/set_home_settings', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        api_token: this.state.api_token,
        id: this.state.id,
        room_id: this.state.room_id,
        set_temp: this.state.set_temp,
        hysteresis_threshold: this.state.hysteresis,
      })
    })
     .then((response) => response.json())
     .then((responseJson) => {
       if (responseJson.meemailssage) this.setState({ error: responseJson.message });
       else if (responseJson.error) this.setState({ error: responseJson.error });
       else {
         this.props.navigation.navigate('Home');
       }
     })
     .catch((error) => { console.error(error) });
  }

  render() {
    return (
      <View style={{ width: width * 0.8, alignSelf: 'center', marginTop: 20}}>
        <Text style={styles.title}>Your Home Configuration</Text>
        <Text style={styles.text}>Wich is your preferred temperature: </Text>
        <TextInput onChangeText={(text) => this.handleChangeTemp(text)} value={this.state.set_temp.toString()} />
        <Text style={styles.text}>Do you want to make sure a specified room is at that temperature? </Text>
        <Switch onValueChange={(value) => this.handleChangeRoomIdBool(value)} value={this.state.room_id_bool}/>
        {this.state.room_id_bool &&
          <View>
            <Text style={styles.text}>Wich?</Text>
            <Picker selectedValue={this.state.room_id || 0} onValueChange={(value) => this.handleChangeRoomId(value)}>
              <Picker.Item label="Select a room" value="0" />
            {this.getRooms()}
            </Picker>
          </View>
        }
        <Text style={styles.text}>Toggle Advanced Settings: </Text>
        <Switch onValueChange={(value) => this.handleToggleAdvancedSettings(value)} value={this.state.advanced_settings}/>
        {this.state.advanced_settings &&
          <View>
            <Text style={styles.text}>Hysteresis Threshold: {this.state.hysteresis}</Text>
            <Slider minimumValue={0} maximumValue={10} onValueChange={(value) => this.handleChangeHysteresis(value)} value={this.state.hysteresis}/>
          </View>
        }
        <View style={{marginTop: 20}}>
          <Button onPress={() => this.handleSend()} title='Apply' />
        </View>
      </View>
    );
  }
};

export default HouseSettings;
