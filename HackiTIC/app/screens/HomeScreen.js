import React, { Component } from 'react';
import { StyleSheet, Text, ScrollView, Button, Dimensions, View, AsyncStorage, TextInput } from 'react-native';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  text: {
    fontSize: 17,
    textAlign: 'center',
  },
  title: {
    fontSize: 27,
    textAlign: 'center',
    marginTop: 30,
  },
});

class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      validUser: false,
      user: false,
      house: false,
      houseText: '',
    }
  }

  async componentDidMount() {
    try {
      let value = await AsyncStorage.getItem('@ApiToken:key');
      if (value !== null){
        this.setState({ validUser: value });

        fetch('https://homenet.erik.cat/api/app/info', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            api_token: value
          })
        })
         .then((response) => response.json())
         .then((responseJson) => {
           if (responseJson.message) console.warn(responseJson.message);
           else if (responseJson.error) console.warn(responseJson.error);
           else {
             this.setState({ user: responseJson.name, house: responseJson.house });
           }
         })
         .catch((error) => { console.error(error); });
      }
    } catch (error) {
      console.error(error.message);
    }
  };

  handleChangeHouseText(text) {
      this.setState({ houseText: text });
  };

  handleNavigateListRooms() {
    this.props.navigation.navigate('ListRooms');
  };

  handleNavigateHouseSettings() {
    this.props.navigation.navigate('HouseSettings');
  };

  handleNavigateLogin() {
    this.props.navigation.navigate('Login');
  };

  handleNavigatePlanATrip() {
    this.props.navigation.navigate('PlanATrip');
  };

  handleSendHouse() {
    fetch('https://homenet.erik.cat/api/app/link_house', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        api_token: this.state.validUser,
        auth_token: this.state.houseText,
      })
    })
     .then((response) => response.json())
     .then((responseJson) => {
       if (responseJson.message) this.setState({ error: responseJson.message });
       else if (responseJson.error) this.setState({ error: responseJson.error });
       else {
         this.props.navigation.navigate('Home');
       }
     })
     .catch((error) => { console.error(error); });
  };

  async handleLogout() {
    try {
      await AsyncStorage.removeItem('@ApiToken:key');
      this.props.navigation.navigate('Home');
    } catch (error) {
      console.error(error.message);
    }
  };

  render() {
    return(
      <ScrollView style={{ width: width * 0.8, alignSelf: 'center', marginTop: 30 }}>
        <Text style={styles.title}>Welcome to the House of the Future{this.state.validUser && ', '}{this.state.user}</Text>
        <View style={{ marginVertical: 20 }}>
          { this.state.validUser ?
             this.state.house ?
              <View>
                <Button onPress={() => this.handleNavigateListRooms()} title='List Rooms' />
                <View style={{ marginTop: 20 }}>
                  <Button onPress={() => this.handleNavigateHouseSettings()} title='House Settings' />
                </View>
                <View style={{ marginTop: 20 }}>
                  <Button onPress={() => this.handleLogout()} title='Logout' />
                </View>
                <View style={{marginTop: 20}}>
                <Button onPress={() => this.handleNavigatePlanATrip()} title='Plan a trip' />
                </View>
              </View>
              :
              <View>
                <Text style={styles.text}>Looks like you haven't any home assigned. Please enter the number that will appear at your Server</Text>
                <TextInput onChangeText={(text) => this.handleChangeHouseText(text)} value={this.state.houseText} />
                <Button onPress={() => this.handleSendHouse()} title='Send' />
                <View style={{ marginTop: 20 }}>
                  <Button onPress={() => this.handleLogout()} title='Logout' />
                </View>
                <View style={{marginTop: 20}}>
                <Button onPress={() => this.handleNavigatePlanATrip()} title='Plan a trip' />
                </View>
              </View>
            :
            <Button onPress={() => this.handleNavigateLogin()} title='Login' />
          }
        </View>
      </ScrollView>
    );
  }
};

export default HomeScreen;
