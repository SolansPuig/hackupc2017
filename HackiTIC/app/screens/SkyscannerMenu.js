import React, { Component } from 'react';
import { StyleSheet, Text, ScrollView, Button, Dimensions, View, AsyncStorage, TextInput, Image } from 'react-native';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  text: {
    fontSize: 17,
    textAlign: 'center',
  },
  title: {
    fontSize: 27,
    textAlign: 'center',
    marginTop: 30,
  },
});

class SkyscannerMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      age: 21,
      market: 'ES',
      currency: 'EUR',
      locale: 'ca-ES',
      outbound: this.props.navigation.state.params.dates[0],
      inbound: this.props.navigation.state.params.dates[1],
      cars: [],
      loading: false,
    }
  }

  /*async componentDidMount() {
    try {
      let value = await AsyncStorage.getItem('@ApiToken:key');
      if (value !== null){
        this.setState({ api_token: value });

        fetch('https://homenet.erik.cat/api/app/info', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            api_token: value,
          })
        })
         .then((response) => response.json())
         .then((responseJson) => {
           if (responseJson.message) console.warn(responseJson.message);
           else if (responseJson.error) console.warn(responseJson.error);
           else {
             this.setState({ age: responseJson.age, market: responseJson.market, currency: responseJson.currency, locale: responseJson.locale });
           }
         })
         .catch((error) => { console.error(error); });
      }
    } catch (error) {
      console.error(error.message);
    }
  }*/

  async connectToCars(pos) {
    let url = 'http://partners.api.skyscanner.net/apiservices/carhire/liveprices/v2/' + this.state.market + '/' + this.state.currency + '/' + this.state.locale + '/' + pos + '/' + pos + '/' + this.state.outbound + '/' + this.state.inbound + '/' + this.state.age + '?apiKey=ha772136595894388989224959580308&userip=147.83.177.115';
    let response = await fetch(url, {body: undefined})
    let Location = await response.headers.get('Location');
    let url2 = 'http://partners.api.skyscanner.net' + Location + '?apiKey=ha772136595894388989224959580308&deltaExcludeWebsites=erac';
    let response2 = await fetch(url2, {body: undefined});
    let responseJson = await response2.json();
    return (responseJson);
  }

  handleGeolocationError(error) {
    console.warn(error.message);
  };

  getMyPosition() {
    return new Promise(
      function (resolve, reject) {
        navigator.geolocation.getCurrentPosition(
          (position) => {
            let latitude = position.coords.latitude.toFixed(2);
            let longitude = position.coords.longitude.toFixed(2);
            let pos = latitude+','+longitude+'-latlong';
            resolve(pos);
          },
          (error) => reject(this.handleGeolocationError(error)),
          { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 },
        )
      }
    );
  }

  async getCarsToRent() {
    this.setState({ loading: true });
    let pos = await this.getMyPosition();
    let responseJson = await this.connectToCars(pos);
    this.setState({ cars: responseJson.cars, loading: false });
  }

  getCars() {
    return this.state.cars.map((car, id) => {
      return(
        <View key={id} style={{ backgroundColor: 'white', width: width * 0.9, alignSelf: 'center', marginVertical: 10 }}>
          <Text style={styles.text}>{car.vehicle}</Text>
          <Text style={styles.text}>{car.price_all_days} €</Text>
          <Text style={styles.text}>At {car.location.pick_up.distance_to_search_location_in_km.toFixed(2)} km</Text>
        </View>
      )
    });
  }

  render() {
    return(
      <ScrollView>
        <View style={{ width: width * 0.8, alignSelf: 'center', marginTop: 30 }}>
          <Text style={styles.text}>All right. I will take care of your home while you are not here.</Text>
          <Text style={styles.text}>Oh, maybe do you want to rent a car, for your trip?</Text>
          <View style={{marginTop: 20}}>
            <Button onPress={() => this.getCarsToRent()} title='See Cars to Rent' />
          </View>
          {this.state.cars && this.getCars()}
          {this.state.loading && <Text style={styles.title}>Loading...</Text>}
        </View>
      </ScrollView>
    );
  }
};

export default SkyscannerMenu;
