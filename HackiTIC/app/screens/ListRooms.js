import React, { Component } from 'react';
import { StyleSheet, Text, ScrollView, Button, Dimensions, View, AsyncStorage, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  text: {
    fontSize: 17,
    textAlign: 'center',
  },
  title: {
    fontSize: 27,
    textAlign: 'center',
    marginBottom: 30,
  },
});

class ListRooms extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rooms: [],
      house: false,
      houseText: '',
    }
  }

  async componentDidMount() {
    try {
      let value = await AsyncStorage.getItem('@ApiToken:key');
      if (value !== null){
        this.setState({ validUser: value });

        fetch('https://homenet.erik.cat/api/app/rooms', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            api_token: value
          })
        })
         .then((response) => response.json())
         .then((responseJson) => {
           if (responseJson.message) console.warn(responseJson.message);
           else if (responseJson.error) console.warn(responseJson.error);
           else {
             this.setState({ rooms: responseJson });
           }
         })
         .catch((error) => { console.error(error); });
      }
    } catch (error) {
      console.error(error.message);
    }
  };

  getRooms() {
    return this.state.rooms.map((room) => {
      let id = room.id;
      return (
        <View key={id} style={{ flex:1, flexDirection: 'row', justifyContent: 'space-between' }}>
          <View style={{ width: '60%', marginVertical: 10 }}>
            <Button onPress={() => this.handlePressRoom(id)} title={room.name} />
          </View>
          <View key={id} style={{ width: '30%', marginVertical: 10 }}>
            <Button onPress={() => this.handleEditRoom(id)} title="Edit" />
          </View>
        </View>
      );
    })
  }

  handlePressRoom(id) {
    this.props.navigation.navigate('Room', { id: id });
  }

  handleEditRoom(id) {
    this.props.navigation.navigate('Edit', { id: id });
  }

  render() {
    return(
      <ScrollView>
        <View style={{ width: width * 0.8, alignSelf: 'center', marginTop: 30 }}>
          <Text style={styles.title}>Here are all your rooms: </Text>
          {this.getRooms()}
        </View>
      </ScrollView>
    );
  }
};

export default ListRooms;
