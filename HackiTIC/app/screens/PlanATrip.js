import React, { Component } from 'react';
import { StyleSheet, Text, ScrollView, Button, Dimensions, View, AsyncStorage, TextInput } from 'react-native';
import PickDate from '../components/PickDate';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  text: {
    fontSize: 17,
    textAlign: 'center',
  },
  title: {
    fontSize: 27,
    textAlign: 'center',
    marginTop: 30,
  },
});

class PlanATrip extends Component {
  constructor(props) {
    super(props);
    this.state = {
      outboundDate: '',
      inboundDate: '',
    }
  }

  handleChangeOutboundDate(date) {
    this.setState({ outboundDate: date});
  }

  handleChangeInboundDate(date) {
    this.setState({ inboundDate: date});
  }

  handleSendHouse() {
    // Send Outbound and Inbound dates to the server
    let dates = [this.state.outboundDate, this.state.inboundDate];
    this.props.navigation.navigate('SkyscannerMenu', {dates: dates});
  };

  render() {
    return(
      <ScrollView style={{ width: width * 0.8, alignSelf: 'center', marginTop: 30 }}>
        <View>
          <Text style={styles.text}>You are planning a trip? I can help you!</Text>
          <Text style={styles.text}>Please, enter an Outbound Date and an Inbound Date.</Text>
          <View style={{marginTop: 20}}>
            <Text style={styles.text}>Outbound Date: </Text>
            <PickDate onRead={(date) => this.handleChangeOutboundDate(date)} />
          </View>
          <View style={{marginVertical: 20}}>
            <Text style={styles.text}>Inbound Date:</Text>
            <PickDate onRead={(date) => this.handleChangeInboundDate(date)} />
          </View>
          <Button onPress={() => this.handleSendHouse()} title='Send' />
        </View>
      </ScrollView>
    );
  }
};

export default PlanATrip;
