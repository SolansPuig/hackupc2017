import React, { Component } from 'react';
import { Text, StyleSheet, Dimensions, Button, View, AsyncStorage, Switch, TextInput, ScrollView, Slider } from 'react-native';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  text: {
    fontSize: 17,
    marginTop: 10,
  },
  title: {
    fontSize: 27,
    textAlign: 'center',
  },
});

class RoomSettings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      api_token: '',
      nameOld: '',
      name: '',
      presence_timeout: '',
      presence_timeout_bool: false,
      precence_activate: '',
      advanced_settings: false,
      light_threshold: 100,
    }
  }

  async componentDidMount() {
    try {
      let value = await AsyncStorage.getItem('@ApiToken:key');
      if (value !== null){
        this.setState({ api_token: value });

        fetch('https://homenet.erik.cat/api/app/room', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            api_token: value,
            id: this.props.navigation.state.params.id,
          })
        })
         .then((response) => response.json())
         .then((responseJson) => {
           if (responseJson.message) console.warn(responseJson.message);
           else if (responseJson.error) console.warn(responseJson.error);
           else {
             this.setState({ nameOld: responseJson.name, name: responseJson.name, presence_timeout: responseJson.presence_timeout.toString(), presence_activate: responseJson.presence_activates_light, light_threshold: responseJson.light_threshold });
             if (this.state.presence_timeout != '0') this.setState({ presence_timeout_bool: true });
             else this.setState({ presence_timeout_bool: false })
           }
         })
         .catch((error) => { console.error(error); });
      }
    } catch (error) {
      console.error(error.message);
    }
  }

  handleChangeName(text) {
    this.setState({ name: text });
  }

  handleChangePresenceActivate(value) {
    this.setState({ presence_activate: value });
  }

  handleChangePresenceTimeoutBool(value) {
    this.setState({ presence_timeout_bool: value });

    if (!value) {
      this.setState({ presence_timeout: '0' });
    }
  }

  handleChangePresenceTimeout(text) {
    this.setState({ presence_timeout: text });
  }

  handleToggleAdvancedSettings(value) {
    this.setState({ advanced_settings: value });
  }

  handleChangeLightThreshold(value) {
    this.setState({ light_threshold: value });
  }

  handleSend() {
     fetch('https://homenet.erik.cat/api/app/set_room_name', {
       method: 'POST',
       headers: {
         'Accept': 'application/json',
         'Content-Type': 'application/json',
       },
       body: JSON.stringify({
         api_token: this.state.api_token,
         id: this.props.navigation.state.params.id,
         name: this.state.name,
       })
     })
      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.meemailssage) this.setState({ error: responseJson.message });
        else if (responseJson.error) this.setState({ error: responseJson.error });
      })
      .catch((error) => { console.error(error); });

      fetch('https://homenet.erik.cat/api/app/set_room_presence_timeout', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          api_token: this.state.api_token,
          id: this.props.navigation.state.params.id,
          presence_timeout: parseInt(this.state.presence_timeout),
        })
      })
       .then((response) => response.json())
       .then((responseJson) => {
         if (responseJson.meemailssage) this.setState({ error: responseJson.message });
         else if (responseJson.error) this.setState({ error: responseJson.error });
       })
       .catch((error) => { console.error(error); });

       fetch('https://homenet.erik.cat/api/app/set_room_presence_activates_light', {
         method: 'POST',
         headers: {
           'Accept': 'application/json',
           'Content-Type': 'application/json',
         },
         body: JSON.stringify({
           api_token: this.state.api_token,
           id: this.props.navigation.state.params.id,
           presence_activates_light: this.state.presence_activate ? 1 : 0,
         })
       })
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.meemailssage) this.setState({ error: responseJson.message });
          else if (responseJson.error) this.setState({ error: responseJson.error });
        })
        .catch((error) => { console.error(error); });

        fetch('https://homenet.erik.cat/api/app/set_room_light_threshold', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            api_token: this.state.api_token,
            id: this.props.navigation.state.params.id,
            light_threshold: this.state.light_threshold,
          })
        })
         .then((response) => response.json())
         .then((responseJson) => {
           if (responseJson.meemailssage) this.setState({ error: responseJson.message });
           else if (responseJson.error) this.setState({ error: responseJson.error });
         })
         .catch((error) => { console.error(error); });

         this.props.navigation.navigate('ListRooms');
   };

  render() {
    return (
      <ScrollView>
        <View style={{ width: width * 0.8, alignSelf: 'center', marginTop: 20}}>
          <Text style={styles.title}>{this.state.nameOld} Configuration</Text>
          <Text style={styles.text}>Change name: </Text>
          <TextInput onChangeText={(text) => this.handleChangeName(text)} value={this.state.name} />
          <Text style={styles.text}>Auto turn on lights when presence detected: </Text>
          <Switch onValueChange={(value) => this.handleChangePresenceActivate(value)} value={this.state.presence_activate == 1 ? true : false}/>
          <Text style={styles.text}>Auto turn off lights when no presence detected (allows to define a timeout): </Text>
          <Switch onValueChange={(value) => this.handleChangePresenceTimeoutBool(value)} value={this.state.presence_timeout_bool}/>
          {this.state.presence_timeout_bool &&
            <View>
              <Text style={styles.text}>Enter a time to wait before close the lights (in seconds): </Text>
              <TextInput onChangeText={(text) => this.handleChangePresenceTimeout(text)} value={this.state.presence_timeout} />
            </View>
          }
          <Text style={styles.text}>Toggle Advanced Settings: </Text>
          <Switch onValueChange={(value) => this.handleToggleAdvancedSettings(value)} value={this.state.advanced_settings}/>
          {this.state.advanced_settings &&
            <View>
              <Text style={styles.text}>Light Threshold: {this.state.light_threshold}</Text>
              <Slider minimumValue={0} maximumValue={1024} onValueChange={(value) => this.handleChangeLightThreshold(value)} value={this.state.light_threshold}/>
            </View>
          }
          <View style={{marginTop: 20}}>
            <Button onPress={() => this.handleSend()} title='Apply' />
          </View>
        </View>
      </ScrollView>
    );
  }
};

export default RoomSettings;
