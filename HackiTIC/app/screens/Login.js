import React, { Component } from 'react';
import { StyleSheet, Text, ScrollView, Button, Dimensions, View, AsyncStorage, TextInput, Slider } from 'react-native';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  text: {
    fontSize: 17,
    marginTop: 10,
  },
  title: {
    fontSize: 27,
    textAlign: 'center',
  },
});

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      error: '',
    }
  }

  handleChangeName(text) {
    this.setState({ email: text });
  }

  handleChangePassword(text) {
    this.setState({ password: text });
  }

  handleSend() {
     fetch('https://homenet.erik.cat/api/app/login', {
       method: 'POST',
       headers: {
         'Accept': 'application/json',
         'Content-Type': 'application/json',
       },
       body: JSON.stringify({
         email: this.state.email,
         password: this.state.password,
       })
     })
      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.message) this.setState({ error: responseJson.message });
        else if (responseJson.error) this.setState({ error: responseJson.error });
        else {
          this.setUser(responseJson.api_token);
        }
      })
      .catch((error) => { console.error(error); });
   };

   async setUser(token) {
     try {
       await AsyncStorage.setItem('@ApiToken:key', token);
       this.props.navigation.navigate('Home');
     } catch (error) {
       console.error(error.message);
     }
   }

  render() {
    return(
      <ScrollView style={{ width: width * 0.8, alignSelf: 'center', marginTop: 20}}>
        <Text style={styles.title}>Login</Text>
        <Text style={styles.text}>Enter your email: </Text>
        <TextInput onChangeText={(text) => this.handleChangeName(text)} value={this.state.name} />
        <Text style={styles.text}>Enter your password: </Text>
        <TextInput onChangeText={(text) => this.handleChangePassword(text)} value={this.state.password} />
        <View style={{ marginVertical: 20 }}>
          <Button onPress={() => this.handleSend()} title='Send' />
        </View>
        <Text style={styles.error}>{this.state.error}</Text>
      </ScrollView>
    );
  }
};

export default Login;
