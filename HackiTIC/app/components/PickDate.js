import React, { Component } from 'react';
import { Button, DatePickerAndroid } from 'react-native';


class PickDate extends Component {
    constructor(props) {
      super(props);
      this.state = {
        date: 'Press here',
      }
    }

    async openDatePicker() {
      try {
        const {action, year, month, day} = await DatePickerAndroid.open({
          date: new Date(),
          minDate: new Date()
        });
        if (action !== DatePickerAndroid.dismissedAction) {
          if (parseInt(day) < 10) day = '0' + day;
          else day = day;
          if (parseInt(month) < 9) month = '0' + (parseInt(month) + 1).toString();
          else month = (parseInt(month) + 1).toString();

          let date = year + '-' + month + '-' + day + 'T08:00';
          this.setState({ date: date });
          this.props.onRead(date);
        }
      } catch ({code, message}) {
        console.warn('Cannot open date picker', message);
      }
    }

    render() {
      return(
        <Button onPress={() => this.openDatePicker()} title={this.state.date} />
      )
    }
}

export default PickDate;
