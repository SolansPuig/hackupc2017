import { StackNavigator } from 'react-navigation';
import HomeScreen from './screens/HomeScreen';
import Login from './screens/Login';
import ListRooms from './screens/ListRooms';
import Room from './screens/Room';
import RoomSettings from './screens/RoomSettings';
import HouseSettings from './screens/HouseSettings';
import PlanATrip from './screens/PlanATrip';
import SkyscannerMenu from './screens/SkyscannerMenu';

const Navigator = StackNavigator({
  Home: { screen: HomeScreen,
    navigationOptions: {
      header: null
    },
  },
  Login: { screen: Login },
  ListRooms: { screen: ListRooms },
  Room: { screen: Room },
  Edit: { screen: RoomSettings },
  HouseSettings: { screen: HouseSettings },
  PlanATrip: { screen: PlanATrip },
  SkyscannerMenu: { screen: SkyscannerMenu },
}, {
  initialRouteName: 'Home'
},
);

export default Navigator;
