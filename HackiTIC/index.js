import { AppRegistry } from 'react-native';
import Navigator from './app/Navigator';

AppRegistry.registerComponent('HackiTIC', () => Navigator);
